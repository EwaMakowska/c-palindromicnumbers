﻿using System;
using System.Linq;

namespace PalindromicNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            String binaryNumber = "100000";
            while(binaryNumber.Contains("0"))
            {
                addOne(ref binaryNumber);
            }

        }

        static void addOne(ref String binaryNumber)
        {
 
            char[] chartArray = binaryNumber.ToCharArray();
            char[] tempArray = binaryNumber.ToCharArray();

            for(int i = 0; i < chartArray.Length; i++) {
                tempArray = binaryNumber.ToCharArray();
                if (tempArray[i].Equals('0'))
                {
                    tempArray[i] = '1';
                    if(isPalindrom(tempArray))
                    {
                        double decimalNumber = binaryToDecimals(tempArray);
                        if(isPalindrom(decimalNumber.ToString().ToCharArray()))
                        {
                            Console.WriteLine(decimalNumber);
                        }
                    }
                    
                }
                if(i+1 < tempArray.Length && tempArray[i+1].Equals('1'))
                {
                    break;
                }

            }
            binaryNumber = new String(tempArray);
        }

        static bool isPalindrom(char[] chartArray)
        {

            char[] tempArray = (char[])chartArray.Clone();
            Array.Reverse(tempArray);
            return Enumerable.SequenceEqual(chartArray, tempArray);
        }

        static double binaryToDecimals(char[] charArray)
        {
            double decimalNumber = 0;
            char[] tempArray = charArray;
            Array.Reverse(tempArray);

            for(int i = 0; i < charArray.Length; i++)
            {
                decimalNumber += Math.Pow(2 * Char.GetNumericValue(tempArray[i]), i);
            }
            return decimalNumber;
        }

    }
}
